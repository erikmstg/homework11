const app = require("../index");
const request = require("supertest");

describe("API /todo", () => {
  it("test get all /todo", (done) => {
    request(app)
      .get("/todo")
      .expect("Content-Type", /json/)
      .expect(200)
      .then((response) => {
        const data = response.body[0];
        expect(data.id);
        expect(data.title);
        expect(data.status);
        done();
      })
      .catch(done);
  });

  it("test get by id /todo/:id", (done) => {
    request(app)
      .get("/todo/3")
      .expect("Content-Type", /json/)
      .expect(200)
      .then((response) => {
        const data = response.body;
        expect(data.id).toBe(3);
        expect(data.title).toBe("Main Game");
        expect(data.status).toBe(true);
        done();
      })
      .catch(done);
  });

  it("test create /todo", (done) => {
    request(app)
      .post("/todo")
      .send({ title: "Berenang", status: true })
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(201)
      .then((response) => {
        const data = response.body;
        expect(data.title).toBe("Berenang");
        expect(data.status).toBe(true);
        done();
      })
      .catch(done);
  });

  it("test delete /todo/:id", (done) => {
    request(app)
      .delete("/todo/2")
      .expect("Content-Type", /json/)
      .expect(200)
      .then((response) => {
        const data = response.body;
        expect(data.message).toBe("Delete success");
        done();
      })
      .catch(done);
  });
});
