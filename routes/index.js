const express = require("express");
const route = express.Router();
const todoRoute = require("./todoRoute");

route.use("/todo", todoRoute);

module.exports = route;
