const { Todo } = require("../models/index");

class TodoController {
  static findAll = async (req, res, next) => {
    try {
      const data = await Todo.findAll();

      res.status(200).json(data);
    } catch (error) {
      next(error);
    }
  };

  static findId = async (req, res, next) => {
    const { id } = req.params;

    try {
      const data = await Todo.findOne({
        where: {
          id,
        },
      });

      if (data) {
        res.status(200).json(data);
      } else {
        throw { name: "errors" };
      }
    } catch (err) {
      next(err);
    }
  };

  static create = async (req, res, next) => {
    try {
      const { title, status } = req.body;

      const data = await Todo.create({
        title,
        status,
      });

      res.status(201).json(data);
    } catch (err) {
      next(err);
    }
  };

  static deleteById = async (req, res, next) => {
    try {
      const { id } = req.params;

      const data = await Todo.update(
        {
          status: false,
        },
        {
          where: {
            id,
          },
        }
      );

      if (data[0] === 1) {
        res.status(200).json({
          message: "Delete success",
        });
      } else {
        throw { name: "errors" };
      }
    } catch (err) {
      next(err);
    }
  };
}

module.exports = TodoController;
