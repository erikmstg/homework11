function errorHandler(err, req, res, next) {
  if (err.name === "errors") {
    res.status(404).json({
      message: "Error not found",
    });
  } else if (err.name === "errorValidation") {
    res.status(400).json({
      message: "Error validation",
    });
  } else {
    res.status(500).json({
      message: "Internal server error",
    });
  }
}

module.exports = errorHandler;
