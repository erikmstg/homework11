const express = require("express");
const route = express.Router();
const TodoController = require("../controllers/todoController");

route.get("/", TodoController.findAll);
route.get("/:id", TodoController.findId);
route.post("/", TodoController.create);
route.delete("/:id", TodoController.deleteById);

module.exports = route;
