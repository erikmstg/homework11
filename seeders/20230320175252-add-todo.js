"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert("Todos", [
      {
        title: "Main Musik",
        status: true,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        title: "Main Game",
        status: true,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        title: "Ngoding",
        status: true,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete("Todos", null, {});
  },
};
